{
  "settingsReloadIntervalMinutes": 1,
  "fullscreen": true,
  "autoStart": true,
  "lazyLoadTabs": false,
  "closeExistingTabs": true,
  "websites": [

    {
      "url": "https://nu.nl",
      "duration": 30,
      "tabReloadIntervalSeconds": 10
    },


    {
      "url": "https://ed.nl",
      "duration": 30,
      "tabReloadIntervalSeconds": 10
    }
  ]
}
